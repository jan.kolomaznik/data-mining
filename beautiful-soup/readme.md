# Zpracování html stránky
Pro zpracování html ale i xml stránek existuje v pythonu knihovna **Beautiful Soup**.
V aktální verzi je pod číslem 4 a proto její naistalování pomocí pip vypadá takto:

```python
!pip install beautifulsoup4
```

Následuje jednoduchý příklad ze stránek dokumentace.

Neprve potřebuje html kód. 
Ten můžeme získat například pomocí knihovny request.
```python
html_doc = """
<html><head><title>The Dormouse's story</title></head>
<body>
<p class="title"><b>The Dormouse's story</b></p>

<p class="story">Once upon a time there were three little sisters; and their names were
<a href="http://example.com/elsie" class="sister" id="link1">Elsie</a>,
<a href="http://example.com/lacie" class="sister" id="link2">Lacie</a> and
<a href="http://example.com/tillie" class="sister" id="link3">Tillie</a>;
and they lived at the bottom of a well.</p>

<p class="story">...</p>
"""
```

Nejprve musím stránku zpracovat pomocí `BeautifulSoup`:
```python
from bs4 import BeautifulSoup
soup = BeautifulSoup(html_doc, 'html.parser')
```

Nyní je možné nejden stránku vytisknout například v hezčím formátování:
```python
print(soup.prettify())
```

Nebo z něj lehce extrahovat data. 
K tomu seoužívá navigace v dokumentu odvozaná od CSS selektorů, což je velmi intuitivní a mocný přístup:
```python
soup.title
# <title>The Dormouse's story</title>
```

```python
soup.title.name
# u'title'
```

```python
soup.title.string
# u'The Dormouse's story'
```

```python
soup.title.parent.name
# u'head'
```

```python
soup.p
# <p class="title"><b>The Dormouse's story</b></p>
```

```python
soup.p['class']
# u'title'
```

```python
soup.a
# <a class="sister" href="http://example.com/elsie" id="link1">Elsie</a>
```

```python
soup.find_all('a')
# [<a class="sister" href="http://example.com/elsie" id="link1">Elsie</a>,
#  <a class="sister" href="http://example.com/lacie" id="link2">Lacie</a>,
#  <a class="sister" href="http://example.com/tillie" id="link3">Tillie</a>]
```

```python
soup.find(id="link3")
# <a class="sister" href="http://example.com/tillie" id="link3">Tillie</a>
```

Běžným úkolem je například najít všechny tagy `<a href=>` a zjisiti kam ukazují:
```python
for link in soup.find_all('a'):
    print(link.get('href'))
```

Nebo ze stránky vytáhnout pouze text:
```python
print(soup.get_text())
```

----
Pro další informace: [How To Scrape Web Pages with Beautiful Soup and Python 3](https://www.digitalocean.com/community/tutorials/how-to-scrape-web-pages-with-beautiful-soup-and-python-3)